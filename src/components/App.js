import React from 'react';

class App extends React.Component {
    render() {
        return (
            <h1>Welcome to CodeLab!!</h1>
        );
    }
}

export default App;
